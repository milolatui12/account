const jwt = require("jsonwebtoken");

exports.generateAccessToken = function(sessionId) {
    return jwt.sign(
        { sessionId },
        process.env.MINIPROGRAM_JWT_SECRETKEY,
        {
            expiresIn: "1h",
        }
    );
}