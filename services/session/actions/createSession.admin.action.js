const { MoleculerError } = require("moleculer").Errors;
const moment = require("moment");

const jwt = require("jsonwebtoken");

const _ = require("lodash");

const SessionConstant = require('../constants/sesion.constant')

module.exports = async function (ctx) {
	try {
		const { accountId, deviceId } = ctx.params;

		await this.broker.call("v1.AdminSessionModel.update", [
			{
				accountId,
				state: SessionConstant.STATE.ACTIVE
			},
			{
				state: SessionConstant.STATE.DEACTIVE
			},
			{
				multi: true
			}
		]);

		let session = await this.broker.call("v1.AdminSessionModel.create", [
			{
				accountId,
				deviceId: "",
				expiredAt: moment(new Date()).add(1, "hour"),
				state: SessionConstant.STATE.ACTIVE
			},
		]);

		if (_.get(session, "id", null) === null) {
			return {
				code: 1001,
				data: {
					message: "fail",
				},
			};
		}

		const accessToken = jwt.sign(
            { sessionId: session.id },
            process.env.MINIPROGRAM_BO_JWT_SECRETKEY,
            {
                expiresIn: "1h",
            }
        );

		session = await this.broker.call("v1.SessionModel.findOneAndUpdate", [
			{
				id: session.id,
			},
			{
				deviceId,
			},
		]);

		return accessToken;
	} catch (error) {
		console.log(error);
		throw new MoleculerError(
			`[MiniProgram] Create session: ${error.message}`
		);
	}
};
