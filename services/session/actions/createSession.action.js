const { MoleculerError } = require("moleculer").Errors;
const moment = require("moment");

const { generateAccessToken } = require("../../../utils/jwt.util");

const _ = require("lodash");

const SessionConstant = require('../constants/sesion.constant')

module.exports = async function (ctx) {
	try {
		const { accountId, deviceId } = ctx.params;

		await this.broker.call("v1.SessionModel.update", [
			{
				accountId,
				state: SessionConstant.STATE.ACTIVE
			},
			{
				state: SessionConstant.STATE.DEACTIVE
			},
			{
				multi: true
			}
		]);

		let session = await this.broker.call("v1.SessionModel.create", [
			{
				accountId,
				deviceId: "",
				expiredAt: moment(new Date()).add(1, "hour"),
				state: SessionConstant.STATE.ACTIVE
			},
		]);

		if (_.get(session, "_id", null) === null) {
			return {
				code: 1001,
				data: {
					message: "fail",
				},
			};
		}

		const accessToken = generateAccessToken(session.id);

		session = await this.broker.call("v1.SessionModel.findOneAndUpdate", [
			{
				id: session.id,
			},
			{
				deviceId,
			},
		]);

		return accessToken;
	} catch (error) {
		console.log(error);
		throw new MoleculerError(
			`[MiniProgram] Create session: ${err.message}`
		);
	}
};
