module.exports = {
	name: "Session",
	version: 1,
	mixins: [],
	settings: {},
	actions: {
		createSession: {
			params: {
				accountId: "number",
				deviceId: "string",
			},
			handler: require("./actions/createSession.action"),
		},
		createAdminSession: {
			params: {
				accountId: "number",
				deviceId: "string",
			},
			handler: require("./actions/createSession.admin.action"),
		},
	},
	methods: {},
	events: {},
	created() {},
	async started() {},
	async stopped() {},
	async afterConnected() {},
	dependencies: [],
};
