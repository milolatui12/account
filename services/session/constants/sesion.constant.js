module.exports = {
    STATE: {
        ACTIVE: "ACTIVE",
        LOGOUT: "LOGOUT",
        DEACTIVE: "DEACTIVE"
    }
}