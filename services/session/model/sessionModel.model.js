const mongoose = require("mongoose");
const autoIncrement = require("mongoose-auto-increment");
const _ = require("lodash");

autoIncrement.initialize(mongoose);

const sessionConstant = require("../constants/sesion.constant");

const Schema = mongoose.Schema(
	{
		id: {
			type: Number,
			required: true,
			unique: true,
		},
		accountId: {
			type: Number,
			require: true,
		},
		deviceId: {
			type: String,
			required: false,
		},
		expiredAt: {
			type: Date,
			required: true,
		},
		state: {
			type: String,
			enum: _.values(sessionConstant.STATE),
		},
	},
	{
		collection: "Session",
		versionKey: false,
		timestamps: true,
	}
);

/*
| ==========================================================
| Plugins
| ==========================================================
*/

Schema.plugin(autoIncrement.plugin, {
	model: `${Schema.options.collection}-id`,
	field: "id",
	startAt: 1,
	incrementBy: 1,
});

/*
| ==========================================================
| Methods
| ==========================================================
*/

/*
| ==========================================================
| HOOKS
| ==========================================================
*/

module.exports = mongoose.model(Schema.options.collection, Schema);
