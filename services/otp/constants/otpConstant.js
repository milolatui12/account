module.exports = {
    TYPE: {
        RESET_PWD: 'RESET_PWD',
        PAY_ORDER: 'PAY_ORDER'
    },
    STATE: {
        ACTIVE: 'ACTIVE',
		DEACTIVE: 'DEACTIVE',
		EXPIRED: 'EXPIRED',
    },
    OPT: {
        OTP_LENGTH: 6,
        OTP_CONFIG: {
            lowerCaseAlphabets: false,
            upperCaseAlphabets: false,
            specialChars: false,
        },
    }
}