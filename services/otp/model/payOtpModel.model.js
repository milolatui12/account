const mongoose = require("mongoose");
const autoIncrement = require('mongoose-auto-increment');

const _ = require("lodash");

autoIncrement.initialize(mongoose);

const OtpConstant = require('../constants/otpConstant');
const otpConstant = require("../constants/otpConstant");

const Schema = mongoose.Schema(
    {
		id: {
			type: Number,
			required: true,
			unique: true,
		},
        accountId: {
            type: Number,
            require: true,
        },
        OTP: {
            type: String,
            require: true,
        },
		type: {
			type: String,
			enum: _.values(OtpConstant.TYPE),
		},
		state: {
			type: String,
			enum: _.values(otpConstant.STATE)
		},
		data: {
			type: Object
		},
		expiredAt: {
			type: Date,
			required: true
		},
        isValid: {
            type: Boolean,
            default: true
        }
	},
	{
		collection: "PayOrderOtp",
		versionKey: false,
		timestamps: true,
	}
);


Schema.plugin(autoIncrement.plugin, {
	model: `${Schema.options.collection}-id`,
	field: 'id',
	startAt: 1,
	incrementBy: 1,
});

module.exports = mongoose.model(Schema.options.collection, Schema);
