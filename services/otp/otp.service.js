module.exports = {
	name: "Otp",
	version: 1,

	mixins: [],

	settings: {
	},
	actions: {
		create: {
			params: {
				accountId: "number",
				data: "object",
				type: "string"
			},
			handler: require("./actions/create.action"),
		},
		verify: {
			params: {
				otp: "string",
                accountId: "number"
			},
			handler: require("./actions/verify.action"),
		},
	},
};
