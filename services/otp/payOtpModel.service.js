const DbService = require("moleculer-db");
const MongooseAdapter = require("moleculer-db-adapter-mongoose");
const MongooseAction = require("moleculer-db-adapter-mongoose-action");

const OtpModel = require('./model/payOtpModel.model')

module.exports = {
	name: "PayOrderOtpModel",
	version: 1,
	mixins: [DbService],

	adapter: new MongooseAdapter(process.env.MONGO_URI, {
		
	}),

	model: OtpModel,

	settings: {},
	actions: MongooseAction(),
	methods: {},
	events: {},
	created() {},
	async started() {},
	async stopped() {},
	async afterConnected() {
		this.logger.info("Connected successfully...");
	},
	dependencies: [],
	// metadata: {
	// 	scalable: true,
	// 	priority: 5,
	// },
};
