const { MoleculerError } = require("moleculer").Errors;

const _ = require("lodash");

const moment = require('moment')

module.exports = async function (ctx) {
	try {
		const { otp, accountId } = ctx.params

        const otpObj = await this.broker.call("v1.PayOrderOtpModel.findOne", [
			{
				OTP: otp,
				accountId,
				isValid: true,
				state: "ACTIVE",
			},
		]);

		const now = new Date();

		if (
			_.get(otpObj, "id", null) === null
		) {
			throw new Error(
                `invalid otp`
            );
		}

		if (
			moment(now).isAfter(otpObj.expiredAt)
		) {
			await this.broker.call("v1.PayOrderOtpModel.updateOne", [
				{
					OTP: otp,
					accountId,
					state: "ACTIVE",
				},
				{
					state: "EXPIRED"
				}
			]);
			throw new Error(
                `otp expired`
            );
		}

		return otpObj
	} catch (error) {
		return {
			code: 1001,
			successed: false,
			message: error.message
		}
	}
};
