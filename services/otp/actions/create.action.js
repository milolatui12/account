const { MoleculerError } = require("moleculer").Errors;

const _ = require("lodash");

const moment = require("moment");

const otpGenerator = require("otp-generator");

const OtpConstant = require("../constants/otpConstant");

module.exports = async function (ctx) {
	try {
		const { accountId, data, type } = ctx.params;

		const otp = otpGenerator.generate(
			OtpConstant.OPT.OTP_LENGTH,
			OtpConstant.OPT.OTP_CONFIG
		);
		await this.broker.call("v1.PayOrderOtpModel.create", [
			{
				accountId,
				OTP: otp,
                data,
				expiredAt: moment(new Date()).add(5, "minute"),
				type,
				state: OtpConstant.STATE.ACTIVE,
			},
		]);

		return otp;
	} catch (error) {
		console.log(error);
		throw new MoleculerError(`[OTP] verify otp: ${error.message}`);
	}
};
