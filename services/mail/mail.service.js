const QueueMixin = require("moleculer-rabbitmq")({
	connection: "amqp://localhost",
	asyncActions: true, // Enable auto generate .async version for actions
});

module.exports = {
	name: "Mail",
	version: 1,

	mixins: [QueueMixin],

	settings: {
		amqp: {
			connection: "amqp://localhost", // You can also override setting from service setting
		},
	},
	actions: {
		send: {
			queue: {
				// Options for AMQP queue
				amqp: {
					queueAssert: {
						durable: true,
					},
					consume: {
						noAck: true,
					},
					prefetch: 1,
				},
			},
			params: {
				subject: "string",
				text: "string",
				email: "string",
			},
			handler: require("./actions/sendMail.action"),
		},
	},
};
