const { MoleculerError } = require("moleculer").Errors;
const nodemailer = require("nodemailer");

const _ = require("lodash");

module.exports = async function (ctx) {
	try {
		const { subject, text, email } = ctx.params;

		const transporter = nodemailer.createTransport({
			service: "Gmail",
			auth: {
				user: process.env.EMAIL_HOST,
				pass: process.env.EMAIL_PASSWORD,
			},
		});
		await transporter.sendMail({
			from: "TRITNT",
			to: email,
			subject: subject,
			text: text,
		});
	} catch (error) {
		console.log(error);
		throw new MoleculerError(`[Send mail] send mail: ${err.message}`);
	}
};
