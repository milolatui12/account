const mongoose = require("mongoose");
const autoIncrement = require('mongoose-auto-increment');

const _ = require("lodash");

autoIncrement.initialize(mongoose);

const Schema = mongoose.Schema(
    {
		id: {
			type: Number,
			required: true,
			unique: true,
		},
        phone: {
            type: String,
            require: true,
            unique: true,
        },
		email: {
			type: String,
			require: false,
		},
		firstName: {
			type: String,
			require: true,
		},
		lastName: {
			type: String,
			require: true,
		},
		password: {
			type: String,
			require: true,
		},
		gender: {
			type: String,
			defalut: null
		},
		avatar: {
			type: String,
			default: null
		},
	},
	{
		collection: "Accounts",
		versionKey: false,
		timestamps: true,
	}
);


Schema.plugin(autoIncrement.plugin, {
	model: `${Schema.options.collection}-id`,
	field: 'id',
	startAt: 1,
	incrementBy: 1,
});

module.exports = mongoose.model(Schema.options.collection, Schema);
