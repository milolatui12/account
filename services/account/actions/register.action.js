const { MoleculerError } = require("moleculer").Errors;
const brcypt = require("bcrypt");
const saltRounds = 10;

const _ = require("lodash");

module.exports = async function (ctx) {
	try {
		const { email, firstName, lastName, phone, password, gender } =
			ctx.params.body;
		const deviceId = ctx.meta.auth.deviceId;

		let accountCreate = await this.broker.call("v1.AccountModel.findOne", [
			{ phone },
		]);

		if (_.get(accountCreate, "_id", null) !== null) {
			return {
				code: 1001,
				successed: false,
				message: "Phone nunber existed!!",
			};
		}

		const hashedPassword = brcypt.hashSync(password, saltRounds);
		const accountObject = {
			phone,
			email,
			firstName,
			lastName,
			gender,
			password: hashedPassword,
		};

		accountCreate = await this.broker.call("v1.AccountModel.create", [
			accountObject,
		]);

		if (_.get(accountCreate, "id", null) === null) {
			return {
				code: 1001,
				successed: false,
				message: "Can't create account",
			};
		}

		const wallet = await this.broker.call("v1.UserWalletModel.create", [
			{
				accountId: accountCreate.id,
				balance: 0,
			},
		]);

		if (_.get(wallet, "id", null) === null) {
			return {
				code: 1001,
				successed: false,
				message: "Can't create account",
			};
		}

		const accessToken = await this.broker.call("v1.Session.createSession", {
			accountId: accountCreate.id,
			deviceId,
		});

		accountCreate.password = undefined;
		return {
			code: 1000,
			successed: true,
			message: "Successfull",
			accountCreate,
			accessToken,
		};
	} catch (error) {
		console.log(error);
		throw new MoleculerError(`[Account] Create account: ${error.message}`);
	}
};
