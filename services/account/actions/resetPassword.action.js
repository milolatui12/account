const { MoleculerError } = require("moleculer").Errors;
const brcypt = require("bcrypt");
const saltRounds = 10;
const moment = require("moment");

const _ = require("lodash");

module.exports = async function (ctx) {
	try {
		const { newPassword } = ctx.params.body;
		const payload = ctx.params.params;

		const token = await this.broker.call("v1.OtpModel.findOne", [
			{
				OTP: payload.token,
			},
		]);

		const now = new Date();

		if (
			_.get(token, "id", null) === null ||
			_.get(token, "isValid") === false ||
			moment(now).isAfter(token.expiredAt)
		) {
			return {
				code: 1001,
				data: {
					message: "invalid link",
				},
			};
		}

		const hashedPassword = brcypt.hashSync(newPassword, saltRounds);

		await this.broker.call("v1.AccountModel.findOneAndUpdate", [
			{
				id: token.accountId,
			},
			{
				password: hashedPassword,
			},
		]);

		await this.broker.call("v1.OtpModel.findOneAndUpdate", [
			{
				id: token.id,
			},
			{ isValid: false },
		]);

		return {
			code: 1000,
			data: {
				message: "Reset Password Successfull",
			},
		};
	} catch (error) {
		console.log(error);
		// if (err.name === "MoleculerError") throw err;
		throw new MoleculerError(
			`[MiniProgram] Reset Password: ${err.message}`
		);
	}
};
