const { MoleculerError } = require("moleculer").Errors;

const _ = require("lodash");

const SessionConstant = require("../../session/constants/sesion.constant");

module.exports = async function (ctx) {
	try {
		const sessionId = ctx.meta.session;
		if (!sessionId) {
			return {
				code: 1000,
				successed: false,
				message: "fail",
			};
		}
		await this.broker.call("v1.SessionModel.findOneAndUpdate", [
			{ id: sessionId },
			{
				state: SessionConstant.STATE.LOGOUT,
			},
		]);

		return {
			code: 1000,
			successed: true,
			message: "Logout Successfull",
		};
	} catch (error) {
		console.log(error);
		// if (err.name === "MoleculerError") throw err;
		throw new MoleculerError(`[Account] logout: ${err.message}`);
	}
};
