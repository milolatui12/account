const { MoleculerError } = require("moleculer").Errors;
const brcypt = require("bcrypt");
const saltRounds = 10;

const { customAlphabet } = require("nanoid");
const { alphanumeric } = require("nanoid-dictionary");
const nanoId = customAlphabet(alphanumeric, 10);
const awaitAsyncForeach = require("await-async-foreach");
const { faker } = require("@faker-js/faker");

const _ = require("lodash");

module.exports = async function (ctx) {
	try {
		const password = "123456";

		await awaitAsyncForeach(Array.from(Array(1000).keys()), async () => {
			const g = ["female", "male"]
			const gender = g[Math.floor(Math.random() * g.length)]

			const firstName = faker.name.firstName(gender)
			const lastName = faker.name.lastName(gender)

			const hashedPassword = brcypt.hashSync(password, saltRounds);
			const accountObject = {
				phone: faker.phone.number('091#######'),
				email: faker.internet.email(firstName, lastName, 'gmail.com'),
				firstName,
				lastName,
				gender,
				password: hashedPassword,
			};
			accountCreate = await this.broker.call("v1.AccountModel.create", [
				accountObject,
			]);

			if (_.get(accountCreate, "id", null) === null) {
				return {
					code: 1001,
					successed: false,
					message: "Can't create account",
				};
			}

			const wallet = await this.broker.call("v1.UserWalletModel.create", [
				{
					accountId: accountCreate.id,
					balance: 0,
				},
			]);

			if (_.get(wallet, "id", null) === null) {
				return {
					code: 1001,
					successed: false,
					message: "Can't create account",
				};
			}
		});
		return "a";
	} catch (error) {
		console.log(error);
		throw new MoleculerError(`[Account] Create account: ${error.message}`);
	}
};
