const { MoleculerError } = require("moleculer").Errors;
const crypto = require("crypto");
const moment = require("moment");

const _ = require("lodash");

module.exports = async function (ctx) {
	try {
		const { phone } = ctx.params.body;

		let account = await this.broker.call("v1.AccountModel.findOne", [
			{ phone },
		]);

		if (_.get(account, "id", null) === null) {
			return {
				code: 1001,
				data: {
					message: "Phone nunber is not exists!!",
				},
			};
		}

		let token = await this.broker.call("v1.OtpModel.findOne", [
			{
				accountId: account.id,
			},
		]);

		if (_.get(token, "id", null) === null) {
			token = await this.broker.call("v1.OtpModel.create", [
				{
					accountId: account.id,
					OTP: crypto.randomBytes(32).toString("hex"),
					expiredAt: moment(new Date()).add(5, "minute"),
				},
			]);
		}

		const link = `http://localhost:3000/v1/reset-password/${token.OTP}`;
		
		await this.broker.call("v1.Mail.send.async", {
			params: {
				subject: "Reset Password Link",
				text: "Your reset password link: " + link,
				email: account.email,
			},
		});


		return {
			code: 1000,
			data: {
				message: "Send mail Successfull",
				link,
			},
		};

	} catch (error) {
		console.log(error);
		// if (err.name === "MoleculerError") throw err;
		throw new MoleculerError(
			`[MiniProgram] Create account: ${err.message}`
		);
	}
};
