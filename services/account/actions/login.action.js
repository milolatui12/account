const { MoleculerError } = require("moleculer").Errors;
const brcypt = require("bcrypt");

const _ = require("lodash");

module.exports = async function (ctx) {
	try {
		const { phone, password } = ctx.params.body;
		const deviceId = ctx.meta.auth.deviceId;
		let account = await this.broker.call("v1.AccountModel.findOne", [
			{ phone },
		]);

		if (_.get(account, "id", null) === null) {
			return {
				code: 1001,
				successed: false,
				message: "Phone nunber is not exists!!",
			};
		}

		const isMatch = brcypt.compareSync(password, account.password);

		if (!isMatch) {
			return {
				code: 1001,
				successed: false,
				message: "Password doesn't match!!",
			};
		}

		// await this.broker.call("v1.SessionModel.update", [
		// 	{
		// 		accountId: account.id,
		// 		state: "ACTIVE",
		// 	},
		// 	{
		// 		state: "DEACTIVE",
		// 	},
		// 	{
		// 		multi: true,
		// 	},
		// ]);

		const accessToken = await this.broker.call("v1.Session.createSession", {
			accountId: account.id,
			deviceId,
		});

		account.password = undefined;
		return {
			code: 1000,
			successed: true,
			message: "Login Successfull",
			account,
			accessToken,
		};
	} catch (error) {
		console.log(error);
		throw new MoleculerError(`[Account] login account: ${error.message}`);
	}
};
