const { MoleculerError } = require("moleculer").Errors;
const AuthMixin = require("../../mixins/authorize.mixin")

const QueueMixin = require("moleculer-rabbitmq")({
	connection: "amqp://localhost",
	asyncActions: true, // Enable auto generate .async version for actions
});

module.exports = {
	name: "Account",
	version: 1,

	mixins: [AuthMixin, QueueMixin],

	settings: {
		amqp: {
			connection: "amqp://127.0.0.1", // You can also override setting from service setting
		},
	},
	actions: {
		register: {
			rest: {
				method: "POST",
				fullPath: "/v1/register",
				auth: {
					strategies: ['default'],
					mode: "optional",
				},
			},
			hooks: {
				before: ["checkDeviceId"],
			},
			params: {
				body: {
					$$type: "object",
					phone: "string",
					email: "string",
					firstName: "string",
					lastName: "string",
					password: "string",
					gender: "string",
				},
			},

			handler: require("./actions/register.action"),
		},
		random: {
			handler: require('./actions/register.random.action')
		},
		login: {
			rest: {
				method: "POST",
				fullPath: "/v1/login",
				auth: {
					strategies: ['default'],
					mode: "optional",
				},
			},
			hooks: {
				before: ["checkDeviceId"],
			},
			params: {
				body: {
					$$type: "object",
					phone: "string",
					password: "string",
				},
			},

			handler: require("./actions/login.action"),
		},
		adminLogin: {
			rest: {
				method: "POST",
				fullPath: "/v1/admin/login",
				auth: {
					strategies: ['bo'],
					mode: "optional",
				},
			},
			hooks: {
				before: ["checkDeviceId"],
			},
			params: {
				body: {
					$$type: "object",
					username: "string",
					password: "string",
				},
			},

			handler: require("./actions/login.admin.action"),
		},
		logout: {
			rest: {
				method: "POST",
				fullPath: "/v1/logout",
				auth: {
					strategies: ['default'],
					mode: "required",
				},
			},
			hooks: {
				before: ["checkIsAuthenticated"],
			},

			handler: require("./actions/logout.action"),
		},
		sendMailResetPassword: {
			rest: {
				method: "POST",
				fullPath: "/v1/reset-password",
			},
			queue: {
				// Options for AMQP queue
				amqp: {
					queueAssert: {
						durable: true,
					},
					prefetch: 1,
				},
			},
			params: {
				body: {
					$$type: "object",
					phone: "string",
				},
			},

			handler: require("./actions/generateResetPasswordLink"),
		},
	},
	methods: {
	},
};
