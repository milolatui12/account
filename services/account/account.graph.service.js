const { MoleculerError } = require("moleculer").Errors;
const _ = require("lodash");
const AuthMixin = require("../../mixins/authorize.mixin");

const QueueMixin = require("moleculer-rabbitmq")({
	connection: "amqp://localhost",
	asyncActions: true, // Enable auto generate .async version for actions
});

module.exports = {
	name: "Account.graph",
	version: 1,

	mixins: [AuthMixin, QueueMixin],

	settings: {
		amqp: {
			connection: "amqp://127.0.0.1", // You can also override setting from service setting
		},
		graphql: {
			type: require("../graph/type"),
			input: require("../graph/input"),
			resolvers: {
				AccountMutation: {
					Login: {
						action: "v1.Account.graph.login",
					},
					Logout: {
						action: "v1.Account.graph.logout",
					},
				},
			},
		},
	},
	actions: {
		hello: {
			graphql: {
				mutation: "AccountMutation: AccountMutation",
			},
			handler(ctx) {
				return "Hello Moleculer!";
			},
		},
		register: {
			hooks: {
				before: ["checkDeviceId"],
			},
			params: {
				body: {
					$$type: "object",
					phone: "string",
					email: "string",
					firstName: "string",
					lastName: "string",
					password: "string",
					gender: "string",
				},
			},

			handler: require("./actions/register.action"),
		},
		login: {
			hooks: {
				before: ["checkDeviceId"],
			},
			params: {
				body: {
					$$type: "object",
					phone: "string",
					password: "string",
				},
			},

			handler: require("./actions/login.action"),
		},
		logout: {
			hooks: {
				before: ["checkIsAuthenticated"],
			},

			handler: require("./actions/logout.action"),
		},
		sendMailResetPassword: {
			queue: {
				// Options for AMQP queue
				amqp: {
					queueAssert: {
						durable: true,
					},
					prefetch: 1,
				},
			},
			params: {
				body: {
					$$type: "object",
					phone: "string",
				},
			},

			handler: require("./actions/generateResetPasswordLink"),
		},
	},
	methods: {},
};
