const gql = require("moleculer-apollo-server").moleculerGql;

module.exports = gql`
	type AccountMutation {
		Login(body: LoginInput!): LoginResponse
		Logout: LogoutResponse
	}
	type LoginResponse {
		successed: Boolean
		accessToken: String
	}

	type LogoutResponse {
		successed: Boolean
		message: String
	}
`;
