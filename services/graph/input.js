const gql = require("moleculer-apollo-server").moleculerGql;

module.exports = gql`
	input LoginInput {
		phone: String!
		password: String!
	}
`;
